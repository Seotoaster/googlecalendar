<?php

class Googlecalendar extends Tools_Plugins_Abstract
{


    public $_test = '';

    public function  __construct($options, $seotoasterData)
    {
        parent::__construct($options, $seotoasterData);
        $this->_view->setScriptPath(dirname(__FILE__) . '/system/views/');
    }

    protected function _init()
    {
        $this->_websiteConfig = Zend_Registry::get('website');
        $this->_themeConfig = Zend_Registry::get('theme');
    }


    public function calendarAction()
    {
        $name = $this->_request->getParam('calname');
        $pieces = explode("/", $name);
        $name = $pieces[0];
        $pageId = $pieces[1];
        $mapper = Application_Model_Mappers_ContainerMapper::getInstance();
        $type = ($pageId) ? Application_Model_Models_Container::TYPE_REGULARCONTENT : Application_Model_Models_Container::TYPE_STATICCONTENT;
        $container = Application_Model_Mappers_ContainerMapper::getInstance()->findByName($name);

        if ($container instanceof Application_Model_Models_Container) {
            $calData = Zend_Json::decode($container->getContent());
        }
        define('GOOGLE_CALENDAR_BASE', 'https://www.google.com/');
        define('GOOGLE_CALENDAR_EMBED_URL', GOOGLE_CALENDAR_BASE . 'calendar/embed');
        $websiteConfig = Application_Model_Mappers_ConfigMapper::getInstance()->getConfig();
        $currentThemePath = realpath(
            $this->_websiteConfig['path'] . $this->_themeConfig['path'] . $websiteConfig['currentTheme']
        );
        if (file_exists($currentThemePath . DIRECTORY_SEPARATOR . 'css' . DIRECTORY_SEPARATOR . 'googlecalendar.css')) {
            $calCustomStyle = Tools_Filesystem_Tools::getFile(
                $currentThemePath . DIRECTORY_SEPARATOR . 'css' . DIRECTORY_SEPARATOR . 'googlecalendar.css'
            );
        } else {
            $calCustomStyle = Tools_Filesystem_Tools::getFile(
                $this->_websiteConfig['path'] . 'plugins' . DIRECTORY_SEPARATOR . 'googlecalendar' . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . 'css' . DIRECTORY_SEPARATOR . 'custom_calendar.css'
            );
        }

        $calCustomStyle = '<style type="text/css">' . $calCustomStyle . '</style>';
        /**
         * Construct calendar URL
         */
        $calendar = $calData['calendar'];
        $language = $calData['language'];

        $calUrl = GOOGLE_CALENDAR_EMBED_URL . '?src=' . $calendar . '&hl=' . $language;
        $calRaw = '';
        if (in_array('curl', get_loaded_extensions())) {
            $curlObj = curl_init();
            curl_setopt($curlObj, CURLOPT_URL, $calUrl);
            curl_setopt($curlObj, CURLOPT_RETURNTRANSFER, 1);
            // trust any SSL certificate (we're only retrieving public data)
            curl_setopt($curlObj, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curlObj, CURLOPT_SSL_VERIFYHOST, false);
            if (function_exists('curl_version')) {
                $curlVer = curl_version();
                if (is_array($curlVer)) {
                    if (!in_array('https', $curlVer['protocols'])) {
                        trigger_error("Can't use https protocol with cURL to retrieve Google Calendar", E_USER_ERROR);
                    }
                    if (!empty($curlVer['version']) &&
                        version_compare($curlVer['version'], '7.15.2', '>=') &&
                        !ini_get('open_basedir') && !ini_get('safe_mode')
                    ) {
                        curl_setopt($curlObj, CURLOPT_FOLLOWLOCATION, 1);
                        curl_setopt($curlObj, CURLOPT_MAXREDIRS, 5);
                    }
                }
            }
            $calRaw = curl_exec($curlObj);
            curl_close($curlObj);
        } else {
            if (ini_get('allow_url_fopen')) {
                if (function_exists('stream_get_wrappers')) {
                    if (!in_array('https', stream_get_wrappers())) {
                        trigger_error("Can't use https protocol with fopen to retrieve Google Calendar", E_USER_ERROR);
                    }
                } else {
                    if (!in_array('openssl', get_loaded_extensions())) {
                        trigger_error("Can't use https protocol with fopen to retrieve Google Calendar", E_USER_ERROR);
                    }
                }
                // fopen should follow HTTP redirects in recent versions
                $fp = fopen($calUrl, 'r');
                while (!feof($fp)) {
                    $calRaw .= fread($fp, 8192);
                }
                fclose($fp);
            } else {
                trigger_error("Can't use cURL or fopen to retrieve Google Calendar", E_USER_ERROR);
            }
        }

        /**
         * Insert BASE tag to accommodate relative paths
         */

        $titleTag = '<title>';
        $baseTag = '<base href="' . GOOGLE_CALENDAR_EMBED_URL . '">';
        $calCustomized = preg_replace("/" . preg_quote($titleTag, '/') . "/i", $baseTag . $titleTag, $calRaw);

        /**
         * Insert custom styles
         */

        $headEndTag = '</head>';
        $calCustomized = preg_replace(
            "/" . preg_quote($headEndTag, '/') . "/i",
            $calCustomStyle . $headEndTag,
            $calCustomized
        );

        /**
         * Extract and modify calendar setup data
         */

        $calSettingsPattern = "(\{\s*window\._init\(\s*)(\{.+\})(\s*\)\;\s*\})";

        if (preg_match("/$calSettingsPattern/", $calCustomized, $matches)) {
            $calSettingsJson = $matches[2];

            $pearJson = null;
            if (!function_exists('json_encode')) {
                // no built-in JSON support, attempt to use PEAR::Services_JSON library
                if (!class_exists('Services_JSON')) {
                    require_once('Services/JSON.php');
                }
                $pearJson = new Services_JSON();
            }

            if (function_exists('json_decode')) {
                $calSettings = json_decode($calSettingsJson);
            } else {
                $calSettings = $pearJson->decode($calSettingsJson);
            }

            // set base URL to accommodate relative paths
            $calSettings->baseUrl = GOOGLE_CALENDAR_BASE;

            // splice in updated calendar setup data
            if (function_exists('json_encode')) {
                $calSettingsJson = json_encode($calSettings);
            } else {
                $calSettingsJson = $pearJson->encode($calSettings);
            }
            // prevent unwanted variable substitutions within JSON data
            // preg_quote() results in excessive escaping
            $calSettingsJson = str_replace('$', '\\$', $calSettingsJson);
            $calCustomized = preg_replace("/$calSettingsPattern/", "\\1$calSettingsJson\\3", $calCustomized);
        }
        header('Content-type: text/html');
        echo $calCustomized;
    }
}