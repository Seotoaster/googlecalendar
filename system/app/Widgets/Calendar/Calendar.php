<?php

class Widgets_Calendar_Calendar extends Widgets_Abstract
{

    protected function _init()
    {
        parent::_init();
        $this->_view = new Zend_View();
        $this->_view->setScriptPath(__DIR__ . '/views/');
        $this->_websiteHelper = Zend_Controller_Action_HelperBroker::getStaticHelper('website');
        $this->_mapper = Application_Model_Mappers_ContainerMapper::getInstance();
    }

    protected function _load()
    {
        $calendarWidth = '580';
        $calendarHeight = '350';

        if (!empty($this->_options[0])) {
            $calendName = $this->_options[0];
            $this->_view->calname = $this->_options[0];
        }
        if (!empty($this->_options[1])) {
            $calendarWidth = $this->_options[1];
        }
        if (!empty($this->_options[2])) {
            $calendarHeight = $this->_options[2];
        }

        $this->_view->calendarWidth = $calendarWidth;
        $this->_view->calendarHeight = $calendarHeight;

        $calData = array(
            'width' => filter_var($this->_options[1], FILTER_SANITIZE_STRING),
            'height' => filter_var($this->_options[2], FILTER_SANITIZE_STRING),
            'calendar' => filter_var($this->_options[3], FILTER_SANITIZE_STRING),
            'language' => filter_var($this->_options[4], FILTER_SANITIZE_STRING)
        );

        $containerName = filter_var($this->_options[0], FILTER_SANITIZE_STRING);
        $pageId = filter_var($this->_toasterOptions['id'], FILTER_SANITIZE_NUMBER_INT);
        $type = ($pageId) ? Application_Model_Models_Container::TYPE_REGULARCONTENT : Application_Model_Models_Container::TYPE_STATICCONTENT;
        $container = $this->_mapper->findByName($containerName, $pageId, $type);

        if ($pageId == 0) {
            $pageId = null;
        }

        if (!$container instanceof Application_Model_Models_Container) {
            $container = new Application_Model_Models_Container();
            $container->setName($containerName)
                ->setPageId($pageId)
                ->setContainerType(
                    ($pageId) ? Application_Model_Models_Container::TYPE_REGULARCONTENT : Application_Model_Models_Container::TYPE_STATICCONTENT
                );
        }
        $container->setContent(Zend_Json::encode($calData));
        $this->_mapper->save($container);
        $this->_view->pageId = $pageId;
        return $this->_view->render('calendar.phtml');
    }

}