<?php

class Widgets_Event_Event extends Widgets_Abstract
{
    protected $_cacheable = false;

    protected function _init()
    {
        parent::_init();
        $this->_view = new Zend_View();
        $this->_view->setScriptPath(__DIR__ . '/views/');
        $this->_websiteHelper = Zend_Controller_Action_HelperBroker::getStaticHelper('website');
        $this->_mapper = Application_Model_Mappers_ContainerMapper::getInstance();
    }

    protected function _load()
    {

        if (empty($this->_options[0]) && empty($this->_options[1])) {
            throw new Exceptions_NewslogException('Not enough parameters passed!');
        }
        $url = simplexml_load_file("https://www.google.com/calendar/feeds/" . $this->_options[1]);
        if ($url->entry) {
            $i = count($url->entry) - 1;
            $event = (string)$url->entry[$i]->summary;
            $str = preg_replace("/(<br>)(.*)|(CEST)|(EEST)|(CET)/", '', $event);
            $pieces = explode(" ", $str);
            $eventDate = strtotime($pieces[2] . " March" . $pieces[4]);
            $date = new Zend_Date($eventDate, Zend_Date::TIMESTAMP);
            $date = ucwords($date->get(Zend_Date::DATE_FULL));

            return $date;
        }
    }

}